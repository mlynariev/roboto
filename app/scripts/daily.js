const config = require('../config/config')
const LoginPage = require('../pages/login');
const UserStoryPage = require('../pages/userstory')
const puppeteerOptions = require('../puppeteer-conf');
const done = process.env.DONE.substr(1, process.env.DONE.length - 2).split(',');
const toDo = process.env.TODO.substr(1, process.env.TODO.length - 2).split(',');
const project = process.env.PROJECT;

let login;
let userStory;

(async () => {
    let { browser, page } = await puppeteerOptions.initialize();

    async function setupPage(page) {
        login = new LoginPage(page);
        userStory = new UserStoryPage(page);
    }

    function closeBrowser() {
        browser.close();
        return 'All done!';
    }

    function selectProject() {
        if (project === '(crypto)') {
            return config.cryptoURL
        } else if (project === '(vectra)') {
            return config.vectraURL
        }
    }


    await setupPage(page);
    await login.navigateToLogin();
    await login.login();
    console.log('*1. Co udało nam się zrobić od ostatniego daily?*');
    for (let i=0; i<done.length; i++) {
        await userStory.navigateToUsPage(selectProject(), done[i]);
        await userStory.getUsTitle(done[i]);
    }
    console.log('*2. Nad czym będziemy pracowali do kolejnego daily?*');
    for (let i=0; i<toDo.length; i++) {
        await userStory.navigateToUsPage(selectProject(), toDo[i]);
        await userStory.getUsTitle(toDo[i]);
    }
    return closeBrowser();
})();