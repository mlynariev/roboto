const vectraURL = 'https://taiga.neoteric.eu/project/karina_cz-herkules/';
const cryptoURL = 'https://taiga.neoteric.eu/project/bwojciechowski-crypto/';
const loginURL = 'https://taiga.neoteric.eu/login/';

const browser = {
    width: 1920,
    height: 1080,
    headless: true,
    slowMo: null
};



module.exports = {
    vectraURL,
    cryptoURL,
    loginURL,
    browser
};
