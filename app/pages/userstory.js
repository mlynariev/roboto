const US_TITLE = '.detail-subject'

class UserStory {
    constructor(page) {
        this.page = page;
    }

    async navigateToUsPage(projectURL, usId) {
        try {
            await this.page.goto(projectURL + 'us/' + usId)
            await this.page.waitForSelector(US_TITLE, { visible: true})
        } catch (e) {
            console.error(e);
            process.exit();

        }
    }

    async getUsTitle(usId) {
        try {
            const innerText = await this.page.$eval(US_TITLE, el => el.innerText);
            console.log('> #', usId, innerText)
        } catch (e) {
            console.error(e);
            process.exit();
        }
    }
}

module.exports = UserStory;