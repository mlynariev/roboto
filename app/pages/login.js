const config = require('../config/config');
const SELECTOR_USERNAME_INPUT = 'input[name="username"]';
const SELECTOR_PASSWORD_INPUT = 'input[name="password"]';
const SELECTOR_SUBMIT_BTN = '.submit-button';

class LoginPage {
    constructor(page) {
        this.page = page;
    }

    async navigateToLogin() {
        try {
            await this.page.goto(config.loginURL);
            console.log('Navigated to login page.');
        } catch (e) {
            console.error(e);
            process.exit();
        }
    }

    async login () {
        try {
            await this.page.waitForXPath('/html/body/div[2]/div/div/div/div/div[2]/form/fieldset[1]/input');
            await this.page.type(SELECTOR_USERNAME_INPUT, process.env.USER);
            await this.page.type(SELECTOR_PASSWORD_INPUT, process.env.PASS);
            await this.page.click(SELECTOR_SUBMIT_BTN);
            await this.page.waitForNavigation();
            console.log('Logged in.');
        } catch (e) {
            console.error(e);
            process.exit();
        }
    }
}

module.exports = LoginPage;